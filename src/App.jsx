import { useState } from 'react'

import './App.css'



function App() {

  let data = ["js", "react", "html","angular", "css","vue","ts", "svelte"];

  const mdata = data.map((item,index) => ({
    name :item,
    selected: false,
    direction: (index%2===0)? 'left' : 'right',
  }))

  const [item, setItem] = useState(mdata);
  const leftData = item.filter((val) => val.direction === 'left');
  const rightData = item.filter((val) => val.direction === 'right');


  const handleCheckboxChange = (ele) => {
    const temp = item.map((item) => {
      if (item.name===ele) {  
        return {
          ...item,
          selected: !item.selected,
        };
      }
      return item;
    });
    setItem(temp);
  }
  const selectedMove = (from,to) => {
    const temp = item.map((item) => {
      if (item.selected && item.direction === from) {
        return {
          ...item,
          selected: false,
          direction: to,
        };
      }
      return item;
    });
    setItem(temp);
  }


  const moveArray=(to)=>{
    const temp = item.map((item) => {
        return{
          ...item,
          direction: to,
        };
    });
    setItem(temp);
  }
  return (
    <>
      <div style={{ display: 'flex' }}>
        <div style={{ width: '500px', height: '500px', border: '2px solid black' }}>
          <ul>
            {leftData.map((ele, index) => {
              return <li key={index}><input checked={ele.selected} onChange={() => handleCheckboxChange(ele.name)} type='checkbox'></input>{ele.name}</li>
            })}
          </ul>
        </div>
        <div style={{ display: "flex", flexDirection: 'column', width: '200px', height: '500px', border: '2px solid black' }}>
          <button onClick={()=>moveArray("left")}>{"<<"}</button>
          <button onClick={()=>selectedMove("right","left")}>{"<"}</button>
          <button onClick={()=>selectedMove("left","right")}>{">"}</button>
          <button onClick={()=>moveArray("right")} >{">>"}</button>
        </div>
        <div style={{ width: '500px', height: '500px', border: '2px solid black' }}>
          <ul>
            {rightData.map((ele, index) => {
              return <li key={index}><input checked={ele.selected} onChange={() => handleCheckboxChange(ele)} type='checkbox'></input>{ele.name}</li>
            })}
          </ul>
        </div>
      </div>
    </>
  )
}

export default App
